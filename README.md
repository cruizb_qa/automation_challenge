# Automation_challenge

# Requirements
* Java 1.8
* Lombok. Please follow its instructions to install it in your favorite IDE
* Google Chrome browser
* ChromeDriver (WebDriver for Chrome) - get it here. (MacOS users just run ./download_chromedriver.sh mac64 and the driver will be downloaded automatically.)
* Mozilla Firefox Web Browser
* GeckoDriver (WebDriver for Firefox)- get it here.
* All drivers you can also find in the drivers folder inside current project.


# Maven
By Default if runs against chrome driver with the command:

$ mvn clean verify