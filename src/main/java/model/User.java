package model;

import lombok.*;
import model.enums.Settings;

@AllArgsConstructor
@Builder
@Getter
public class User extends Settings {

    private final String email;
    private final String password;
    private final String newPassword;
    private final String name;
    private final String surName;
    private final Settings.language language;
    private final String country;

    public String getLanguage() {
        return languageStringHashMap.get(language);
    }
}
