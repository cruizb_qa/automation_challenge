package model.enums;

import java.util.HashMap;

public class Settings {
    public enum language {
        EN,
        ES,
        FR,
        IT,
        PO,
        DE,
        CH,
        JA
    }

    public HashMap<language, String> languageStringHashMap = new HashMap<language, String>();

    public Settings() {
        languageStringHashMap.put(language.EN, "english");
        languageStringHashMap.put(language.ES, "español");
        languageStringHashMap.put(language.FR, "français");
        languageStringHashMap.put(language.IT, "italiano");
        languageStringHashMap.put(language.DE, "deustch");
        languageStringHashMap.put(language.PO, "português (brasil)");
        languageStringHashMap.put(language.CH, "chinese");
        languageStringHashMap.put(language.JA, "japanese");
    }
}
