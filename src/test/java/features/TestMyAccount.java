package features;

import model.User;
import model.enums.Settings;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import steps.LoginSteps;
import steps.MainDashboardAssertSteps;
import steps.MainDashboardSteps;

import static net.thucydides.core.annotations.ClearCookiesPolicy.BeforeEachTest;

@RunWith(SerenityRunner.class)
public class TestMyAccount {

    @Managed(clearCookies=BeforeEachTest)
    WebDriver browser;

    @Steps
    LoginSteps loginSteps;

    @Steps
    MainDashboardSteps mainDashboardSteps;

    @Steps
    MainDashboardAssertSteps mainDashboardAssertSteps;

    private User user;

    @Test
    @Title("Access to settings without Premium account")
    public void accessToSettingWithoutPremiumAccount() {
        user = User.builder()
                .email("cruizbqa@gmail.com")
                .name("Cesar QA")
                .language(Settings.language.EN)
                .country("Spain")
                .build();

        mainDashboardAssertSteps.arePremiumAdviceElementsShown();
        mainDashboardAssertSteps.areUserDataShown(user);
    }

    @Test
    @Title("Change user name")
    public void changeUserName() {
        user = User.builder()
                .name("Cesar")
                .surName("QA")
                .build();

        mainDashboardSteps.goToChangeUserName();
        mainDashboardSteps.eraseForm();
        mainDashboardSteps.submitEditName();
        mainDashboardAssertSteps.userNameIsMandatory();
        mainDashboardSteps.fillUserNameForm(user);
        mainDashboardSteps.submitEditName();
        mainDashboardAssertSteps.areUserDataShown(user);
    }

    @Test
    @Title("Change language")
    public void changeLanguage() {
        user = User.builder()
                .language(Settings.language.EN)
                .build();

        mainDashboardSteps.goToChangeLanguage();
        mainDashboardSteps.selectLanguage(user.getLanguage());
        mainDashboardSteps.submitLanguageForm();
        mainDashboardAssertSteps.areUserDataShown(user);
    }

    @Test
    @Title("Change email")
    public void changeEmail() {
        user = User.builder()
                .email("cruizbqa2@gmail.com")
                .password("qateam")
                .build();

        User userRestore = User.builder()
                .email("cruizbqa@gmail.com")
                .password("qateam")
                .build();

        mainDashboardSteps.goToChangeEmail();
        mainDashboardSteps.submitEmailForm();
        mainDashboardAssertSteps.areAllEmailFieldsMandatory();
        mainDashboardSteps.fillEmailForm(user);
        mainDashboardSteps.submitEmailForm();
        mainDashboardAssertSteps.areUserDataShown(user);

        //Restore user data
        mainDashboardSteps.goToChangeEmail();
        mainDashboardSteps.fillEmailForm(userRestore);
        mainDashboardSteps.submitEmailForm();
        mainDashboardAssertSteps.areUserDataShown(userRestore);
    }

    @Test
    @Title("Change password")
    public void changePassword() {
        user = User.builder()
                .password("qateam")
                .newPassword("qateam")
                .build();

        mainDashboardSteps.goToChangePassword();
        mainDashboardSteps.submitPasswordForm();
        mainDashboardAssertSteps.areAllPasswordFieldsMandatory();
        mainDashboardSteps.fillPasswordFields(user);
        mainDashboardSteps.submitPasswordForm();
    }

    @Test
    @Title("Change country")
    public void changeCountry() {
        user = User.builder()
                .country("Spain")
                .build();

        mainDashboardSteps.goToChangeCountry();
        mainDashboardAssertSteps.countrySelectedIsEqualsTo(user.getCountry());
        mainDashboardSteps.selectCountryOfList(user.getCountry());
        mainDashboardSteps.submitCountryForm();
        mainDashboardAssertSteps.areUserDataShown(user);
    }

    @Before
    @Title("Access to settings with user")
    public void accessToSettingsWithUser() {
        user = User.builder()
                .email("cruizbqa@gmail.com")
                .password("qateam")
                .build();

        loginSteps.loginUser(user);
        mainDashboardSteps.goToSettings();
    }
}
