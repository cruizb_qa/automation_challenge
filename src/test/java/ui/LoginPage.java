package ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.By;

@DefaultUrl("https://family.qustodio.com/parents-app#/")
public class LoginPage extends PageObject {

    private final By LOGIN_CONTENT = By.cssSelector(".Login__content");
    private final By USER = By.cssSelector(".Form__input[type='email']");
    private final By PASSWORD = By.cssSelector(".Form__input[type='password']");
    private final By LOGIN = By.cssSelector(".rck-btn");

    @WhenPageOpens
    private  void waitUntilPageLoad() {
        find(LOGIN_CONTENT).waitUntilVisible();
    }

    public void fillUserName(String name) {
        find(USER).sendKeys(name);
    }

    public void fillPassword(String password) {
        find(PASSWORD).sendKeys(password);
    }

    public void ClickOnLogin() {
        find(LOGIN).click();
    }
}
