package ui;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

public class EditSettingsPage extends PageObject {

    private final By NAME_FORM =  By.cssSelector(".EditName");
    private final By EDIT_NAME_FORM = By.cssSelector(".rck-text-field__grouped input");
    private final By SUBMIT_EDIT_NAME = By.cssSelector("button[form='editName']");
    private final By SUBMIT_EDIT_EMAIL = By.cssSelector("button[form='editEmail']");
    private final By SUBMIT_COUNTRY = By.cssSelector("button[data-testid='Button__saveEditCountry']");
    private final By SUBMIT_PASSWORD_EMAIL = By.cssSelector("button[form='editPassword']");
    private final By SUBMIT_LANGUAGE_NAME = By.cssSelector("[data-testid='Button__saveEditLanguage']");
    private final By NAME_FIELD = By.cssSelector("#SaveNameButton");
    private final By SURNAME_FIELD = By.cssSelector("#SaveLastNameButton");
    private final By NAME_MESSAGE = By.cssSelector("[for='name'].rck-info-field-label__label--mode-error");
    private final By NEW_EMAIL_MESSAGE = By.cssSelector("[for='email'].rck-text-field--error");
    private final By REPEAT_EMAIL_MESSAGE = By.cssSelector("[for='confirmEmail'].rck-info-field-label__label--mode-error");
    private final By PASSWORD_MESSAGE = By.cssSelector("[for='password'].rck-info-field-label__label--mode-error");
    private final By NEW_PASSWORD_MESSAGE = By.cssSelector("[for='passwordNew'].rck-info-field-label__label--mode-error");
    private final By REPEAT_NEW_PASSWORD_MESSAGE = By.cssSelector("[for='confirmPasswordNew'].rck-info-field-label__label--mode-error");
    private final By LANGUAGE_DROP = By.cssSelector(".rck-action-input");
    private final By OPTIONS_LIST = By.cssSelector(".rck-dropdown-option");
    private final By NEW_EMAIL = By.id("SaveEmailButton");
    private final By REPEAT_EMAIL = By.id("ConfirmEmailButton");
    private final By PASSWORD = By.id("PonfirmPasswordButtonId");
    private final By CURRENT_PASSWORD = By.cssSelector("input[name='password']");
    private final By NEW_PASSWORD = By.cssSelector("input[name='passwordNew']");
    private final By REPEAT_NEW_PASSWORD = By.cssSelector("input[name='confirmPasswordNew']");
    private final By COUNTRY = By.cssSelector(".rck-text-field__input");
    private final By DROPDOWN_LIST = By.cssSelector(".rck-dropdown__list");

    public void waitUntilEditNameIsShown() {
        find(NAME_FORM).waitUntilVisible();
    }

    public void eraseUserNameField() {
        findAll(EDIT_NAME_FORM)
                .stream()
                .forEach(field-> field.clear());
    }

    public void clickOnSubmitEditName() {
        find(SUBMIT_EDIT_NAME).click();
    }

    public boolean isNameFieldMandatoryMessageShown() {
        return find(NAME_MESSAGE).isCurrentlyVisible();
    }

    public void fillNameField(String name) {
        fillIfParamIsNotEmpty(NAME_FIELD, name);
    }

    public void fillSurNameField(String surName) {
        fillIfParamIsNotEmpty(SURNAME_FIELD, surName);
    }

    private void fillIfParamIsNotEmpty(By selector, String data) {
        if (!data.isEmpty()) {
            find(selector).sendKeys(data);
        }
    }

    public void downloadLanguages() {
        find(LANGUAGE_DROP).click();
    }

    public void selectLanguageByText(String language) {
        selectOptionByText(language);
    }

    public void clickOnSubmitLanguageForm() {
        find(SUBMIT_LANGUAGE_NAME).click();
    }

    public void clickOnSubmitEmailForm() {
        find(SUBMIT_EDIT_EMAIL).click();
    }

    public boolean isNewEmailFieldMandatoryMessageShown() {
        return find(NEW_EMAIL_MESSAGE).isCurrentlyVisible();
    }

    public boolean isRepeatEmailFieldMandatoryMessageShown() {
        return find(REPEAT_EMAIL_MESSAGE).isCurrentlyVisible();
    }

    public boolean isPasswordFieldMandatoryMessageShown() {
        return find(PASSWORD_MESSAGE).isCurrentlyVisible();
    }

    public void fillNewEmail(String newEmail) {
        find(NEW_EMAIL).sendKeys(newEmail);
    }

    public void fillRepeatEmail(String newEmail) {
        find(REPEAT_EMAIL).sendKeys(newEmail);
    }

    public void fillPassword(String password) {
        find(PASSWORD).sendKeys(password);
    }

    public void clickOnSubmitPassword() {
        find(SUBMIT_PASSWORD_EMAIL).click();
    }

    public boolean isNewPasswordFieldMandatoryMessageShown() {
        return find(NEW_PASSWORD_MESSAGE).isCurrentlyVisible();
    }

    public boolean isRepeatNewPasswordFieldMandatoryMessageShown() {
        return find(REPEAT_NEW_PASSWORD_MESSAGE).isCurrentlyVisible();
    }

    public void fillNewPassword(String newPassword) {
        find(NEW_PASSWORD).sendKeys(newPassword);
    }

    public void fillCurrentPassword(String password) {
        find(CURRENT_PASSWORD).sendKeys(password);
    }

    public void fillRepeatNewPassword(String newPassword) {
        find(REPEAT_NEW_PASSWORD).sendKeys(newPassword);
    }

    public String getCountry() {
        return find(COUNTRY).getValue();
    }

    public void fillCountryField(String country) {
        find(COUNTRY).click();
        find(COUNTRY).sendKeys(country);
    }

    public void selectCountryByText(String country) {
        find(DROPDOWN_LIST).waitUntilVisible();
        selectOptionByText(country);
    }

    public void clickOnSubmitCountry() {
        find(SUBMIT_COUNTRY).click();
    }

    private void selectOptionByText(String option) {
        findAll(OPTIONS_LIST)
                .stream()
                .filter(options->options.getText().equalsIgnoreCase(option))
                .findFirst()
                .get()
                .click();
    }
}
