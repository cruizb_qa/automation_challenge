package ui;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;

public class MainDashboardPage extends PageObject {

    private final By HEADER = By.cssSelector(".DesktopLayout__header-container");
    private final By NAVIGATION_MENU = By.cssSelector(".rck-side-navigation-menu");
    private final By MY_ACCOUNT = By.cssSelector(".rck-side-navigation-menu__anchor[data-testid='SideNavigationMenuAnchor__Your Account']");
    private final By PREMIUM = By.cssSelector(".rck-btn");
    private final By NAME = By.cssSelector("button[data-testid='ActionInput__fullNameAccountSetting']");
    private final By PASSWORD = By.cssSelector("button[data-testid='ActionInput__passwordAccountSetting']");
    private final By EMAIL = By.cssSelector("button[data-testid='ActionInput__emailAccountSetting']");
    private final By LANGUAGE = By.cssSelector("button[data-testid='ActionInput__languageAccountSetting']");
    private final By COUNTRY = By.cssSelector("button[data-testid='ActionInput__countryAccountSetting']");
    private final By DAILY_EMAIL = By.id("isDailyReport");
    private final By WEEKLY_EMAIL = By.id("isWeeklyReport");

    public void waitUntilPageLoaded() {
        find(HEADER).waitUntilVisible();
        find(NAVIGATION_MENU).waitUntilVisible();
    }

    public void clickOnSettings() {
        find(MY_ACCOUNT).click();
    }

    public boolean isPremiumButtonEnabled() {
        return find(PREMIUM).isDisplayed() &
                find(PREMIUM).isClickable();
    }

    public String getUserName() {
        return getText(NAME);
    }

    public String getEmail() {
        return getText(EMAIL);
    }

    public String getLanguage() {
        return getText(LANGUAGE);
    }

    public String getCountry() {
        return getText(COUNTRY);
    }

    private String getText(By selector) {
        return find(selector).getText();
    }

    public void clickOnName() {
        find(NAME).click();
    }

    public void clickOnLanguage() {
        find(LANGUAGE).click();
    }

    public void clickOnEmail() {
        find(EMAIL).click();
    }

    public void clickOnPassword() {
        find(PASSWORD).click();
    }

    public void clickOnCountry() {
        find(COUNTRY).click();
    }

    public boolean isDailyNotificationChecked() {
        return find(DAILY_EMAIL).isSelected();
    }

    public boolean isWeeklyNotificationChecked() {
        return find(WEEKLY_EMAIL).isSelected();
    }
}
