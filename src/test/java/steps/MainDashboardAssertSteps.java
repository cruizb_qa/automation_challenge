package steps;

import model.User;
import net.thucydides.core.annotations.Step;
import ui.EditSettingsPage;
import ui.MainDashboardPage;

import static org.assertj.core.api.Assertions.assertThat;

public class MainDashboardAssertSteps {

    MainDashboardPage mainDashboardPage;
    EditSettingsPage editSettingsPage;

    @Step("Are Premium Advice Elements Shown")
    public void arePremiumAdviceElementsShown() {
        assertThat(mainDashboardPage.isPremiumButtonEnabled()).isTrue();
    }

    @Step("Are user data shown")
    public void areUserDataShown(User user) {
        String _fullName = user.getName();
        if (user.getSurName() != null) {
            _fullName = String.join(" ", user.getName(), user.getSurName());
        }
        validateIfNotNull(mainDashboardPage.getUserName(), _fullName);
        validateIfNotNull(mainDashboardPage.getEmail(), user.getEmail());
        validateIfNotNull(mainDashboardPage.getLanguage(), user.getLanguage());
        validateIfNotNull(mainDashboardPage.getCountry(), user.getCountry());
        assertThat(mainDashboardPage.isDailyNotificationChecked()).isTrue();
        assertThat(mainDashboardPage.isWeeklyNotificationChecked()).isFalse();
    }

    @Step("User name is mandatory")
    public void userNameIsMandatory() {
        assertThat(editSettingsPage.isNameFieldMandatoryMessageShown()).isTrue();
    }

    @Step("Are all email fields mandatory")
    public void areAllEmailFieldsMandatory() {
        assertThat(editSettingsPage.isNewEmailFieldMandatoryMessageShown());
        assertThat(editSettingsPage.isRepeatEmailFieldMandatoryMessageShown());
        assertThat(editSettingsPage.isPasswordFieldMandatoryMessageShown());
    }

    @Step("Are all password fields mandatory")
    public void areAllPasswordFieldsMandatory() {
        assertThat(editSettingsPage.isPasswordFieldMandatoryMessageShown());
        assertThat(editSettingsPage.isNewPasswordFieldMandatoryMessageShown());
        assertThat(editSettingsPage.isRepeatNewPasswordFieldMandatoryMessageShown());
    }

    @Step("Country Selected is equals to {0}")
    public void countrySelectedIsEqualsTo(String country) {
        assertThat(editSettingsPage.getCountry()).isEqualToIgnoringCase(country);
    }

    private void validateIfNotNull(String actual, String expected) {
        if (expected != null) {
            assertThat(actual).isEqualToIgnoringCase(expected);
        }
    }
}
