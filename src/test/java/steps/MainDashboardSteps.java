package steps;

import model.User;
import net.thucydides.core.annotations.Step;
import ui.EditSettingsPage;
import ui.MainDashboardPage;

public class MainDashboardSteps {

    MainDashboardPage mainDashboardPage;
    EditSettingsPage editSettingsPage;

    @Step("Go to Settings")
    public void goToSettings() {
        mainDashboardPage.clickOnSettings();
    }

    @Step("Go to change user name")
    public void goToChangeUserName() {
        mainDashboardPage.clickOnName();
        editSettingsPage.waitUntilEditNameIsShown();
    }

    @Step("Erase form")
    public void eraseForm() {
        editSettingsPage.eraseUserNameField();
    }

    @Step("Submit edit name")
    public void submitEditName() {
        editSettingsPage.clickOnSubmitEditName();
    }

    @Step("Fill user name Name form {0}")
    public void fillUserNameForm(User user) {
        editSettingsPage.fillNameField(user.getName());
        editSettingsPage.fillSurNameField(user.getSurName());
    }

    @Step("Go to Change language")
    public void goToChangeLanguage() {
        mainDashboardPage.clickOnLanguage();
    }

    @Step("Select language {0}")
    public void selectLanguage(String language) {
        editSettingsPage.downloadLanguages();
        editSettingsPage.selectLanguageByText(language);
    }

    @Step("Submit language form")
    public void submitLanguageForm() {
        editSettingsPage.clickOnSubmitLanguageForm();
    }

    @Step("Go to change email")
    public void goToChangeEmail() {
        mainDashboardPage.clickOnEmail();
    }

    @Step("Submit email form")
    public void submitEmailForm() {
        editSettingsPage.clickOnSubmitEmailForm();
    }

    @Step("Fill email form {0}")
    public void fillEmailForm(User user) {
        editSettingsPage.fillNewEmail(user.getEmail());
        editSettingsPage.fillRepeatEmail(user.getEmail());
        editSettingsPage.fillPassword(user.getPassword());
    }

    @Step("Go to change password")
    public void goToChangePassword() {
        mainDashboardPage.clickOnPassword();
    }

    @Step("Submit password form")
    public void submitPasswordForm() {
        editSettingsPage.clickOnSubmitPassword();
    }

    @Step("Fill password fields")
    public void fillPasswordFields(User user) {
        editSettingsPage.fillCurrentPassword(user.getPassword());
        editSettingsPage.fillNewPassword(user.getNewPassword());
        editSettingsPage.fillRepeatNewPassword(user.getNewPassword());
    }

    @Step("Go to Change country")
    public void goToChangeCountry() {
        mainDashboardPage.clickOnCountry();
    }

    @Step("Select country of list")
    public void selectCountryOfList(String country) {
        editSettingsPage.fillCountryField(country);
        editSettingsPage.selectCountryByText(country);
    }

    @Step("Submit country form")
    public void submitCountryForm() {
        editSettingsPage.clickOnSubmitCountry();
    }
}
