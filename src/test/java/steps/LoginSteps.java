package steps;

import ui.LoginPage;
import model.User;
import net.thucydides.core.annotations.Step;
import ui.MainDashboardPage;

public class LoginSteps {

    LoginPage loginPage;
    MainDashboardPage mainDashboardPage;

    @Step("Login user {0}")
    public void loginUser(User user) {
        loginPage.open();
        loginPage.fillUserName(user.getEmail());
        loginPage.fillPassword(user.getPassword());
        loginPage.ClickOnLogin();
        mainDashboardPage.waitUntilPageLoaded();
    }
}
